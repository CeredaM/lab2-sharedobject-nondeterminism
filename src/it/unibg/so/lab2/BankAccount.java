package it.unibg.so.lab2;

public class BankAccount {
	
	private int balance;
	
	public BankAccount(int initialBalance){
		this.balance = initialBalance;
	}

	public void deposit(int value){
		int lastBalance = balance;
		int newBalance = lastBalance + value;
		balance = newBalance;
	}
	
	public int withdraw(int value){
		if(balance >= value){
			int lastBalance = balance;
			int newBalance = lastBalance - value;
			balance = newBalance;
			return value;
		}
		return -1;
	}
	
	public int getBalance(){
		return balance;
	}
	
	public static void main(String[] args){
		BankAccount account = new BankAccount(1000);
	
		Thread a = new ThreadA(account);
		Thread b = new ThreadB(account);
		a.start();
		b.start();
		
		try {
			a.join();
			b.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Balance: " + account.getBalance());
	}

}
