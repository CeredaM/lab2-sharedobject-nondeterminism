package it.unibg.so.lab2;

public class ThreadA extends Thread {

	private BankAccount sharedAccount;
	
	public ThreadA(BankAccount account){
		sharedAccount = account;
	}

	@Override
	public void run() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		sharedAccount.withdraw(100);
	}	
}
